const path = require('path')
const fs = require('fs')

const express = require('express')
const app = express()
const port = 3000
const appRoot = path.join(__dirname, '..', 'public')

// formidable is an express plugin allows us to easily parse multipart forms
const formidable = require('express-formidable')
const uploadPath = path.join(__dirname, '..', 'public', 'uploads')

const Ticket = require('./ticket/ticket')

if (!fs.existsSync(uploadPath)){
    fs.mkdirSync(uploadPath);
}

app.use(formidable({
    encoding: 'utf-8',
    keepExtensions: true,
    uploadDir: uploadPath,
    multiples: true,
    maxFileSize: 2 * 1024 * 1024
}))

// serve up the form from the root
app.use('/', express.static(appRoot, {
    index: 'index.html'
}))

// handle an HTTP POST on our ticket route
app.post('/api/ticket', (req, res) => {
    const newTicket = Ticket.createNew(req.fields.excavatorName)

    // express-formidable has a slightly weird API for working with attachments...
    // maybe multer is a nice alternative? https://www.npmjs.com/package/multer
    if (Array.isArray(req.files.attachment)) {
        newTicket.attachments = req.files.attachment.map(fileUpload => ({
            name: fileUpload.name,
            path: fileUpload.path
        }))
    }
    else if (req.files.attachment.size) {
        newTicket.attachments = [{
            name: req.files.attachment.name,
            path: req.files.attachment.path
        }]
    }

    if (req.query.redirect === 'true') {
        // see https://en.wikipedia.org/wiki/List_of_HTTP_status_codes#3xx_Redirection
        const redirectHttpCode = 303
        const redirectDestinationUrl = `http://localhost:3000?ticket=${newTicket.ticket}&revision=${newTicket.revision}&expiration=${newTicket.expirationDate}`

        return res.redirect(redirectHttpCode, redirectDestinationUrl)
    }
    else {
        const stringifiedTicketResult = JSON.stringify(newTicket, null, 0)

        res.setHeader('Content-Type', 'application/json')
        res.status(201).send(stringifiedTicketResult)
    }
})

app.listen(port, () => console.log(`Example app listening on port ${port}!`))

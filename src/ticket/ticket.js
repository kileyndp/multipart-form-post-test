class Ticket {
    constructor(excavator) {
        this.ticket = this.getNextTicketNumber()
        this.revision = this.getNextRevisionNumber()
        this.expirationDate = this.calculateExpirationDate()
        this.excavator = excavator
    }

    getNextTicketNumber() {
        return 'A000000000'
    }

    getNextRevisionNumber() {
        return '00A'
    }

    calculateExpirationDate() {
        const daysUntilExpiration = 28
        let expirationDate = new Date()
        expirationDate.setDate(expirationDate.getDate() + daysUntilExpiration)

        return expirationDate
    }

    static createNew(excavatorName = 'UNKNOWN') {
        let excavator = {
            name: excavatorName
        }

        return new Ticket(excavator)
    }
}

module.exports = Ticket

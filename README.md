# posttest

This project is a test to experiment with options for receiving multipart/form data.

### Prerequisites

```
Node v10.11.0+
NPM v6.4.1+
```

### Installing

Install with NPM

```
npm install
```

When running, server will output a message:

```
Example app listening on port 3000!
```

Open a browser to `http://localhost:3000`. Then:

* Use the top form to submit a multipart/form-data POST to the API and get back an **AJAX-style response**
* Use the bottom form to submit a multipart/form-data POST to the API and get back a **redirect response** with URL params



## Authors

* **Kiley Naro** - *Initial work* - [BitBucket](https://bitbucket.org/kileyndp)
